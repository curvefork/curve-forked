from brownie import Registry, Contract, accounts

from scripts.get_pool_data import get_pool_data
from scripts.utils import get_gas_price, pack_values

# modify this prior to mainnet use
DEPLOYER = None # accounts.at("0x7EeAC6CDdbd1D0B8aF061742D41877D7F707289a", force=True)
DEPLOYER="0xE71978b1696a972b1a8f724A4eBDB906d9dA0885"
REGISTRY = "0x3d6dD34166AD57017D6033ccacFFb53C0096b14D"


data = ...

#def add_pool(data, registry, deployer):

swap = Contract(data['swap_address'])
token = data['lp_token_address']
n_coins = len(data['coins'])
decimals = pack_values([i.get('decimals', i.get('wrapped_decimals')) for i in data['coins']])

is_v1 = data['lp_contract'] == "CurveTokenV1"
has_initial_A = hasattr(swap, 'intitial_A')
rate_method_id = "0x00"


use_lending_rates = pack_values(["wrapped_decimals" in i for i in data['coins']])
registry.add_pool_without_underlying(
    swap,
    n_coins,
    token,
    rate_method_id,
    decimals,
    use_lending_rates,
    has_initial_A,
    is_v1,
    {'from': deployer, 'gas_price': get_gas_price()}
)


