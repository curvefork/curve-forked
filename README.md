
## Curve pool forked contracts  
## Existing deploys

### Rinkeby
[New3Poll-its realy work!!](https://rinkeby.etherscan.io/address/0xf9b5c66a61b211935611722d31077a68415bcd91#code)  
with  
[lptoken](https://rinkeby.etherscan.io/address/0x4B37E248A526914B590f127b1C302A94Cb3251D4#code)
for 3 StableCoins (3Poll) ( 
[DAI](https://rinkeby.etherscan.io/address/0x3E40865Ce782a363162d5E3208a18cFa609CDf00#code)
[USDC](https://rinkeby.etherscan.io/address/0xFD398ee18686Db31D324366Acb04CB0fc591Af50#code)
[USDT](https://rinkeby.etherscan.io/address/0xFD398ee18686Db31D324366Acb04CB0fc591Af50#code))


### Test Binance Smart Chain
[TESTNET Binance (BNB) Blockchain Explorer](https://testnet.bscscan.com/)  
[DAI test token](https://testnet.bscscan.com/address/0x136ac25c56b24e5e117d71ff74c62d8539d2ee29#code)  
[USDC test token](https://testnet.bscscan.com/address/0x109cb1f5a4851104c9db27188c8158035d3303c1#code)  
[USDT test token](https://testnet.bscscan.com/address/0x20e30c7c1295fcd1a78528078b83aaf16c5ce032#code)  
[PlatinumLPToken](https://testnet.bscscan.com/address/0xb22e29aff453fb1d72503044cabecd6be1136a7d#code)  
[Platinum3pool](https://testnet.bscscan.com/address/0xa9656955be3d65e679fd1b5dc87c68699b67c377#code)   

### Binance Smart Chain
to be soon

-----

### Links
https://testnet.binance.org/faucet-smart  
https://github.com/nvm-sh/nvm#usage  
https://eth-brownie.readthedocs.io/en/stable/network-management.html   
https://academy.binance.com/ru/articles/connecting-metamask-to-binance-smart-chain