#For add inf inPoolRegistry
{"3pool":
    {
        "lp_contract": "CurveTokenV2",
        "swap_address": "0xF9b5C66A61b211935611722d31077a68415bCd91",
        "lp_token_address": "0x4B37E248A526914B590f127b1C302A94Cb3251D4",
        "coins": [
            {
                "name": "DAI",
                "decimals": 18,
                "tethered": False,
                "underlying_address": "0x3E40865Ce782a363162d5E3208a18cFa609CDf00"
            },
            {
                "name": "USDC",
                "decimals": 6,
                "tethered": False,
                "underlying_address": "0xFD398ee18686Db31D324366Acb04CB0fc591Af50"

            },
            {
                "name": "USDT",
                "decimals": 6,
                "tethered": True,
                "underlying_address": "0xBCE39120BafEc001E16890945960151DE49Da313"
            }
        ]
    }
}